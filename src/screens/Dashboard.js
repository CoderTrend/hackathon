import React, { memo } from 'react';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Button from '../components/Button';

const Dashboard = ({ navigation }) => (
  <Background>
    <Logo />
    <Header>Trang chủ</Header>
    <Paragraph>
      Nhấp nhô theo từng nhịp điệu nè =))
    </Paragraph>
    <Button mode="outlined" onPress={() => navigation.navigate('HomeScreen')}>
      Đăng xuất
    </Button>
  </Background>
);

export default memo(Dashboard);
