export const emailValidator = email => {
  const re = /\S+@\S+\.\S+/;

  if (!email || email.length <= 0) return 'Email không được để trống!';
  if (!re.test(email)) return 'Email không hợp lệ!';

  return '';
};

export const passwordValidator = password => {
  if (!password || password.length <= 0) return 'Mật khẩu không được để trống!';

  return '';
};

export const nameValidator = name => {
  if (!name || name.length <= 0) return 'Tên không được để trống!';

  return '';
};
